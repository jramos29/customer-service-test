const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../server');

const should = chai.should();

chai.use(chaiHttp);


/* eslint-disable no-undef */
describe('Books', () => {
/*
  * Test the /GET route
  */
  describe('/GET books', () => {
    it('it should GET all the books', (done) => {
      chai.request(server)
        .get('/api/books')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          done();
        });
    });
  });
});
