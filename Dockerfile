#Get Windows instance with Node Installed Bambu Specific
FROM node:carbon

#NPM Install Package libs
ADD package.json ./ 
RUN npm install

#Add all other files to the working directory
ADD . ./

#Expose Port of Choice
EXPOSE 8080

WORKDIR ./

#Take Note it is 'start' and not 'npm start' as the entrypoint is taken as npm
CMD ["npm","start"]

